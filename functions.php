<?php 

//---------------------------------------------------------------------
// REGISTRAMOS EL MENU
//---------------------------------------------------------------------

register_nav_menus( array(
	'menu-principal' => __('Area principal de navegación', 'amk')
) );



//---------------------------------------------------------------------
// CARACTERÍSTICAS DEL TEMA
//---------------------------------------------------------------------

// Ajustar el máximo ancho de las imagenes de acuerdo al diseño de este modo cualquier imagen que insertemos en el contenido de un artículo va a tener como máximo este ancho
if ( ! isset( $content_width ) )
	$content_width = 750; //El ancho máximo será de 750 pixeles

// Creamos una función para registrar algunas características del tema
function amk_theme_features()  {

	// Permitimos que el sitio soporte RSS Automáticos
	add_theme_support( 'automatic-feed-links' );

	// Permitimos qe el tema soporte imagenes destacadas
	add_theme_support( 'post-thumbnails');	
}

// Ejecutamos la función y registra las características
add_action( 'after_setup_theme', 'amk_theme_features' );




//---------------------------------------------------------------------
// REGISTRAMOS EL SIDEBAR
//---------------------------------------------------------------------

//Con la función register_sidebar, registramos una zona dinámica para nuestro tema y le pasamos algunos parámetros
register_sidebar(array(
	'name' => __('Bara lateral', 'amk'), //El nombre del área dinámica
	'id' => 'barra-lateral', //Un identificador único para la zona
	'description' => __( 'Este es el área de widgets del sitio.', 'amk'), //Una breve descripción
	'before_widget' => '<div id="%1$s" class="widget %2$s">', //Algo de HTML que irá antes de cada widget
	'after_widget'  => '</div>', //Algo de HTML que irá después de cada widget
	'before_title' => '<h3>', //La etiqueta que irá antes del título de cada widget
	'after_title' => '</h3>' //La etiqueta que irá después del título de cada widget
));





//---------------------------------------------------------------------
// CARGANDO ESTILOS DEL TEMA
//---------------------------------------------------------------------
//Creamos una función para cargar los estilos
function amk_theme_styles() { 

	//Registramos la fuente Open Sans
	wp_register_style( 'font-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700', '', '', 'all' );
	
	//Registramos Bootstrap
	wp_register_style( 'bootstrap', get_stylesheet_directory_uri().'/css/bootstrap.min.css', '', '3.0.0', 'all' );
	
	//registramos la hoja de estilos del tema
	wp_register_style( 'amk-style', get_stylesheet_uri(), array('font-sans', 'bootstrap'), '1.0.0', 'all' );
	
	
	
	//Ahora cargamos los estilos. Nota que sólo cargamos 'amk-style' ya que en esta hoja de estilos declaramos dependendencia de 'font-sans' y 'botstrap', éstas cargaran de manera automática
	wp_enqueue_style( 'amk-style' );
}
add_action('wp_enqueue_scripts', 'amk_theme_styles'); //Ejecutamos la función





//---------------------------------------------------------------------
// CARGANDO EL JAVASCRIPT DEL TEMA
//---------------------------------------------------------------------
//Creamos una función para cargar los scripts
function amk_theme_scripts() {
	
	//Registramos el script de bootstrap
	wp_register_script( 'bootstrap', get_stylesheet_directory_uri().'/js/bootstrap.min.js', array('jquery'), '3.0.0', true );
	
	//Registramos el script de personalizado del tema
	wp_register_script( 'amk-scripts', get_stylesheet_directory_uri().'/js/amk-scripts.js', array( 'jquery', 'bootstrap' ), '2.1', true );
	
	
	//Ahora cargamos el script del tema.Como declaramos dependendencia de 'jquery' y 'bootstrap', éstas cargaran de manera automática
	wp_enqueue_script( 'amk-scripts' );
}

add_action( 'wp_enqueue_scripts', 'amk_theme_scripts' ); //Ejecutamos la función


?>