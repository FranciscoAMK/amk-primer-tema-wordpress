<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	
	<title><?php wp_title('|', true, 'right');?> <?php bloginfo('name'); ?></title>
	
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	
	<!-- Cargando los íconos  -->
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/apple-touch-icon-152x152.png" />
	
	<!-- Pingbacks -->
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
	
	<?php wp_head(); ?>
	

</head>

<body <?php body_class(); ?>>
	
	
	<!-- #MAIN HEADER  -->
	<header id="main-header">
		
		<div class="container">
			<div class="row">
				
				<!-- Título del sitio  -->	
				<div id="logo" class="col-md-6">
					<h1><?php bloginfo('name');?><br>
					<small><?php bloginfo('description'); ?></small></h1>
				</div>
				
				<div class="col-md-6">
					<?php wp_nav_menu(array(
						'theme_location'  => 'menu-principal',
						'container'       => 'nav',
						'items_wrap'      => '<ul id="%1$s" class="%2$s nav nav-pills">%3$s</ul>'
					)); ?> 
				</div>
				
			</div>
			
			<hr />
		</div>
		
	</header>
	<!-- #MAIN HEADER  -->