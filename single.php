<?php get_header(); ?>

<section class="container">
	
	<div class="row">
		
		<div class="col-md-8">
			
			<?php //El loop básicamente comprueba si hay posts para mostrar, luego mientras haya posts cargará cada uno de ellos usando el esquema que se ve a continuación
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			
				<article <?php post_class('clearfix'); //Carga las clases específicas del post y agrega la clase clearfix ?>>
					
					<header>
						<h1><?php the_title(); //Muestra el título del post ?></h1>
						<div class="meta">
							<?php the_time(get_option('date_format')); //Muestra la fecha de publicación del artículo ?>
							&bull;
							<?php the_category(', '); //Muestra enlaces a las categorías separados por coma ?></div>
					</header>
					
					<?php //En esta parte muestra el contenido del post
					the_content();
					
					//Muestra una paginación si es que el post tiene la etiqueta <!--nextpage-->
					wp_link_pages(array(
						'before'           => '<p class="paginacion">' . __('Páginas', 'apk')
					));
					
					
					//Muestra las etiquetas asociadas al artículo
					the_tags(__('Etiquetas: ', 'apk'), ', '); ?>
					
					
				</article>
				
				<hr />
			
			
			<?php endwhile; //Acá termina la estructura con la que se presentará cada post
			else: //Ahora bien, si no hay artículos para mostrar entonces cargará lo siguiente  ?>
			
				<article>
					
					<header>
						<h1><?php _e('Este contenido no está disponible', 'amk'); //Un simple título ?></h1>
					</header>
					
					<?php get_search_form(); //Llama un formulario de búsqueda  ?>
				
				</article>
			
			<?php endif; // Aquí termina el loop?>
			
			
			<?php //Cargamos la plantilla de comentarios
			comments_template(); ?>
			
			
		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
		
		
	</div>
	
</section>

<?php get_footer(); ?>